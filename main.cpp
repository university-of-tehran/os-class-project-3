#include <iostream>
#include <vector>
#include <fstream>
#include <time.h>
#include <math.h>
#include <sstream>
#include <ctime>
#include <chrono>

using namespace std;

int total_emission = 0;

struct road
{
    char entrance_node;
    char exit_node;
    int h;
    int road_emission_till_now;
    bool occupied = false;
};

struct car
{
    vector<char> nodes;
    int p;
    int path_number;
    int car_number;
    vector<string> log;
};

void read_from_input_file(vector<road> &roads, vector<car> &cars)
{
    ifstream MyInputFile("in.txt");
    string input;
    string input2;
    int num_of_cars;
    int path_number;
    srand(time(0));

    while (1)
    {
        getline(MyInputFile, input);
        if (input == "#" || MyInputFile.eof())
        {
            break;
        }
        roads.push_back(road());
        roads[roads.size() - 1].entrance_node = input[0];
        roads[roads.size() - 1].exit_node = input[2];
        roads[roads.size() - 1].h = input[4] - '0';
    }
    path_number = 0;
    while (1)
    {
        path_number++;
        getline(MyInputFile, input);
        if (input == "#" || MyInputFile.eof())
        {
            break;
        }

        getline(MyInputFile, input2);
        num_of_cars = input2[0] - '0';
        for (int i = 0; i < num_of_cars; i++)
        {
            cars.push_back(car());
            cars[cars.size() - 1].p = rand() % 10 + 1;
            cars[cars.size() - 1].car_number = i + 1;
            cars[cars.size() - 1].path_number = path_number;
            for (int j = 0; j < input.size(); j += 2)
            {
                cars[cars.size() - 1].nodes.push_back(input[j]);
            }
        }
    }
    MyInputFile.close();
}

int calculate_the_formula(int p, int h)
{
    int result = 0;
    for (int k = 0; k <= 10000000; k++)
    {
        result += floor(k / (1000000) / p / h);
    }
    return result;
}

void write_car_log_to_file(car &car)
{
    string fileName = "";
    string s = "";
    string s2 = "";
    stringstream ss;
    stringstream ss2;
    ss << car.path_number;
    ss >> s;
    fileName += s;
    fileName += "-";
    ss2 << car.car_number;
    ss2 >> s2;
    fileName += s2;
    fileName += ".txt";

    ofstream myfile;
    myfile.open(fileName);
    for (int i = 0; i < car.log.size(); i++)
    {
        myfile << car.log[i];
    }
    myfile.close();
}

void add_to_car_log(car &car, road road, chrono::high_resolution_clock::time_point entrance_time, chrono::high_resolution_clock::time_point exit_time, int emission)
{
    string logLine = "";
    string entrance_t = "";
    string exit_t = "";
    string emission_s = "";
    string total_emission_s = "";
    stringstream emission_ss;
    stringstream total_emission_ss;

    // Calculate times
    std::time_t t = chrono::high_resolution_clock::to_time_t(entrance_time);
    entrance_t = ctime(&t);
    t = chrono::high_resolution_clock::to_time_t(exit_time);
    exit_t = ctime(&t);
    exit_t.pop_back();
    entrance_t.pop_back();

    // calculate emissions
    emission_ss << emission;
    emission_ss >> emission_s;
    total_emission_ss << emission + total_emission;
    total_emission_ss >> total_emission_s;
    logLine = "a";
    logLine[0] = road.entrance_node;
    logLine += "," + entrance_t + "," + road.exit_node + "," + exit_t + "," + emission_s + "," + total_emission_s + "\n";
    car.log.push_back(logLine);
}

int do_stuff_for_car(car &car, vector<road> &roads)
{
    int emission = 0;

    if (car.nodes.size() < 2)
    {
        return 0;
    }
    for (int i = 0; i < roads.size(); i++)
    {
        if (roads[i].entrance_node == car.nodes[0] and roads[i].exit_node == car.nodes[1])
        {
            if (roads[i].occupied)
            {
                return 0;
            }
            else
            {
                roads[i].occupied = true;
                chrono::high_resolution_clock::time_point entrance_time = chrono::high_resolution_clock::now();
                emission = calculate_the_formula(car.p, roads[i].h);
                chrono::high_resolution_clock::time_point exit_time = chrono::high_resolution_clock::now();
                car.nodes.erase(car.nodes.begin());
                if (car.nodes.size() == 1)
                {
                    car.nodes.erase(car.nodes.begin());
                }

                add_to_car_log(car, roads[i], entrance_time, exit_time, emission);

                roads[i].occupied = false;
                return emission;
            }
        }
    }
    return 0;
}

int main()
{
    vector<road> roads;
    vector<car> cars;
    car current_car;
    int done_cars = 0;
    cout << "Reading the input file." << endl;
    read_from_input_file(roads, cars);
    cout << "Calculating... this may take a while." << endl;
    while (1)
    {
        done_cars = 0;
        for (int i = 0; i < cars.size(); i++)
        {
            if (cars[i].nodes.size() == 0)
            {
                done_cars++;
            }
            else
            {
                total_emission += do_stuff_for_car(cars[i], roads);
            }
        }
        if (done_cars == cars.size())
        {
            break;
        }
    }
    cout << "Writing to the output files." << endl;

    for (int i = 0; i < cars.size(); i++)
    {
        write_car_log_to_file(cars[i]);
    }
    cout << "Done." << endl;
    return 0;
}